Rails.application.routes.draw do
  root 'orders#index'
  resources :employee_territories
  resources :customer_customer_demographics
  resources :order_details
  resources :customer_demographics
  resources :territories
  resources :regions
  resources :orders
  resources :employees
  resources :customers
  resources :categories
  resources :shippers
  resources :suppliers
  resources :products
end
