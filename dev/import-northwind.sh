#!/bin/bash

# import-northwind.sh
#
# Imports Northwind database to PostgreSQL server
# Creates database user for current system user
#    with password 'northwind'


# Delete older version of db if exists
#   using PostgreSQL dropdb command line tool
dropdb northwind

# Create database user for current user
#   using SQL syntax via psql command
sudo -u postgres psql -c \
  "CREATE ROLE IF NOT EXIST $USER WITH CREATEDB INHERIT LOGIN ENCRYPTED PASSWORD 'northwind'"

# create db
createdb northwind

# import
sed 's/postgres/'$USER'/g' dev/northwind.postgre.sql > dev/northwind.postgre.sql.$USER
psql northwind -f dev/northwind.postgre.sql.$USER

# check if tables exist
# sudo -u postgres
psql northwind -c '\d'
