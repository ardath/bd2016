# notatki do projektu bazy danych 2016

# z upla:
Celem projektu jest zaimplementowanie systemu realizującego wybrane podstawowe operacje w przykładowej bazie Northwind w wybranej technologii.
np.
Operacje CRUD na wybranych tabelach
Operacje składania zamówień na produkty
Operacje wyszukiwania informacji i raportowania (realizacja złożonych operacji SQL)
itp.
Należy zwrócić uwagę na sposób i wydajność realizacji operacji bazodanowych (np. przedstawić wyniki pomiarów wydajnościowych)

Ważnym elementem projektu jest prezentacja technologii w której realizowany jest projekt, dokumentacja powinna mieć formę przewodnika (tutorialu) po danej technologii (przewodnik ilustrujący jak należy programować elementy systemu bazodanowego - na przykładzie bazy Northwind), przewodnik powinien zawierać opis wraz z elementami kodu

Dokumentację należy prowadzić na bieżąco (wiki)
Kod powinien być dostępny w repozytorium (svn, git)
Ostateczną wersję projektu należy oddać w formie w pełni skonfigurowanej maszyny wirtualnej (vmware lub virtualbox)
Technologia realizacji projektu do uzgodnienia z prowadzącym

Propozycje technologii:
Java, Hibernate
Ruby on Rails
Java, Play Framework
Python Django
ASP.NET
C#, ADO.NET, LINQ
PHP, Cake PHP
PHP Zend
Aplikacja mobilna, Android
Aplikacja mobilna, Windows Phone
Inne do uzgodnienia

Proponowane SZBD:
MS SQL Server
Oracle
Postgres
MySql
MongoDB
Inne do uzgodnienia
