#!/bin/bash

# generate_scaffolds.sh.sh
# Generates scaffolds for Northwind database utilizing rails built-in generator

updating=${u:-0}

if [[ $updating == 0 ]]; then

bundle exec rails generate scaffold Shipper CompanyName:string Phone:string --migration false

bundle exec rails generate scaffold Category CategoryName:string Description:string Picture:string --migration false

bundle exec rails generate scaffold Customer CompanyName:string ContactName:string ContactTitle:string Address:string City:string Region:string PostalCode:string Country:string Phone:string Fax:string --migration false

bundle exec rails generate scaffold Employee LastName:string FirstName:string Title:string TitleOfCourtesy:string BirthDate:date HireDate:date Address:string City:string Region:string PostalCode:string Country:string HomePhone:string Extension:string Photo:string Notes:string PhotoPath:string --migration false

bundle exec rails generate scaffold Order OrderDate:date, ShippedDate:date, Freight:float ShipName:string ShipAddress:string ShipCity:string ShipRegion:string ShipPostalCode:string ShipCountry:string --migration false

bundle exec rails generate scaffold Product ProductName:string QuantityPerUnit:string UnitPrice:float UnitsInStock:integer UnitsOnOrder:integer ReorderLevel:integer Discontinued:integer --migration false

bundle exec rails generate scaffold Region RegionDescription:string --migration false

bundle exec rails generate scaffold Supplier CompanyName:string ContactName:string ContactTitle:string Address:string City:string Region:string PostalCode:string Country:string Phone:string Fax:string HomePage:string --migration false

bundle exec rails generate scaffold Territory TerritoryDescription:string --migration false

?bundle exec rails generate scaffold Usstate StateName:string StateAbbr:string StateRegion:string --migration false

bundle exec rails generate scaffold CustomerDemographic CustomerDesc:string --migration false

bundle exec rails generate scaffold OrderDetail UnitPrice:decimal Quantity:integer Discount:decimal --migration false

bundle exec rails generate scaffold CustomerCustomerDemographic Customer:references CustomerDemographic:references --migration false

fi

bundle exec rails generate scaffold EmployeeTerritory Employee:references Territory:references --migration false
