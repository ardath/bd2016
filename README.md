### Strona projektu - bazy danych 2016.


Ruby on Rails, Postgresql

https://bitbucket.org/ardath/bd2016

wiki: https://bitbucket.org/ardath/bd2016.git/wiki

### Opis projektu
Celem projektu jest implementacja systemu realizującego operacje w przykładowej
bazie Northwind z uzyciem frameworka Ruby on Rails na bazie Postgresql.

System bedzie realizowal nastepujace operacje:

* Operacje CRUD na wybranych tabelach

* Operacje składania zamówień na produkty

* Operacje wyszukiwania informacji i raportowania (realizacja złożonych operacji SQL)

Dokumentacja rozwoju systemu bedzie prowadzona na stronach wiki.

Dokumentacja bedzie zawierac informacje jak zainstalowac i skonfigurowac system,
tak zeby mogla go zainstalowac samodzielnie nawet osoba niemajaca wczesniej kontaktu z wybranymi technologiami.

Oprocz tego z pomoca wiki uzytkownik bedzie mogl sie zapoznac z procesem rozwijania systemu.

W szczegolnosci beda zawarte nastepujace informacje:

* jake sa niezbedne do dzialania systemu pakiety i jak je zainstalowac w wybranym systemie operacyjnym

* jak skonfigurowac srodowisko uzytkownika do wspolpracy z systemem

* jak zainstalowac i skonfigurowac system

* jak byly dodawane kolejne funkcjonalnosci (wprowadzenie do Ruby on Rails)

TODO: dodac polskie znaki...