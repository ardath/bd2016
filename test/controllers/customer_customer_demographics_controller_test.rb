require 'test_helper'

class CustomerCustomerDemographicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @customer_customer_demographic = customer_customer_demographics(:one)
  end

  test "should get index" do
    get customer_customer_demographics_url
    assert_response :success
  end

  test "should get new" do
    get new_customer_customer_demographic_url
    assert_response :success
  end

  test "should create customer_customer_demographic" do
    assert_difference('CustomerCustomerDemographic.count') do
      post customer_customer_demographics_url, params: { customer_customer_demographic: { CustomerType_id: @customer_customer_demographic.CustomerType_id, Customer_id: @customer_customer_demographic.Customer_id } }
    end

    assert_redirected_to customer_customer_demographic_url(CustomerCustomerDemographic.last)
  end

  test "should show customer_customer_demographic" do
    get customer_customer_demographic_url(@customer_customer_demographic)
    assert_response :success
  end

  test "should get edit" do
    get edit_customer_customer_demographic_url(@customer_customer_demographic)
    assert_response :success
  end

  test "should update customer_customer_demographic" do
    patch customer_customer_demographic_url(@customer_customer_demographic), params: { customer_customer_demographic: { CustomerType_id: @customer_customer_demographic.CustomerType_id, Customer_id: @customer_customer_demographic.Customer_id } }
    assert_redirected_to customer_customer_demographic_url(@customer_customer_demographic)
  end

  test "should destroy customer_customer_demographic" do
    assert_difference('CustomerCustomerDemographic.count', -1) do
      delete customer_customer_demographic_url(@customer_customer_demographic)
    end

    assert_redirected_to customer_customer_demographics_url
  end
end
