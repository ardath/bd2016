# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161124083922) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", primary_key: "CategoryID", id: :integer, limit: 2, force: :cascade do |t|
    t.string "CategoryName", limit: 15, null: false
    t.text   "Description"
    t.binary "Picture"
  end

  create_table "customercustomerdemo", primary_key: ["CustomerID", "CustomerTypeID"], force: :cascade do |t|
    t.string "CustomerID",     null: false
    t.string "CustomerTypeID", null: false
  end

  create_table "customerdemographics", primary_key: "CustomerTypeID", id: :string, force: :cascade do |t|
    t.text "CustomerDesc"
  end

  create_table "customers", primary_key: "CustomerID", id: :string, force: :cascade do |t|
    t.string "CompanyName",  limit: 40, null: false
    t.string "ContactName",  limit: 30
    t.string "ContactTitle", limit: 30
    t.string "Address",      limit: 60
    t.string "City",         limit: 15
    t.string "Region",       limit: 15
    t.string "PostalCode",   limit: 10
    t.string "Country",      limit: 15
    t.string "Phone",        limit: 24
    t.string "Fax",          limit: 24
  end

  create_table "databases", force: :cascade do |t|
  end

  create_table "employees", primary_key: "EmployeeID", id: :integer, limit: 2, force: :cascade do |t|
    t.string  "LastName",        limit: 20,  null: false
    t.string  "FirstName",       limit: 10,  null: false
    t.string  "Title",           limit: 30
    t.string  "TitleOfCourtesy", limit: 25
    t.date    "BirthDate"
    t.date    "HireDate"
    t.string  "Address",         limit: 60
    t.string  "City",            limit: 15
    t.string  "Region",          limit: 15
    t.string  "PostalCode",      limit: 10
    t.string  "Country",         limit: 15
    t.string  "HomePhone",       limit: 24
    t.string  "Extension",       limit: 4
    t.binary  "Photo"
    t.text    "Notes"
    t.integer "ReportsTo",       limit: 2
    t.string  "PhotoPath",       limit: 255
  end

  create_table "employeeterritories", primary_key: ["EmployeeID", "TerritoryID"], force: :cascade do |t|
    t.integer "EmployeeID",  limit: 2,  null: false
    t.string  "TerritoryID", limit: 20, null: false
  end

  create_table "order_details", primary_key: ["OrderID", "ProductID"], force: :cascade do |t|
    t.integer "OrderID",   limit: 2, null: false
    t.integer "ProductID", limit: 2, null: false
    t.float   "UnitPrice",           null: false
    t.integer "Quantity",  limit: 2, null: false
    t.float   "Discount",            null: false
  end

  create_table "orders", primary_key: "OrderID", id: :integer, limit: 2, force: :cascade do |t|
    t.string  "CustomerID"
    t.integer "EmployeeID",     limit: 2
    t.date    "OrderDate"
    t.date    "RequiredDate"
    t.date    "ShippedDate"
    t.integer "ShipVia",        limit: 2
    t.float   "Freight"
    t.string  "ShipName",       limit: 40
    t.string  "ShipAddress",    limit: 60
    t.string  "ShipCity",       limit: 15
    t.string  "ShipRegion",     limit: 15
    t.string  "ShipPostalCode", limit: 10
    t.string  "ShipCountry",    limit: 15
  end

  create_table "products", primary_key: "ProductID", id: :integer, limit: 2, force: :cascade do |t|
    t.string  "ProductName",     limit: 40, null: false
    t.integer "SupplierID",      limit: 2
    t.integer "CategoryID",      limit: 2
    t.string  "QuantityPerUnit", limit: 20
    t.float   "UnitPrice"
    t.integer "UnitsInStock",    limit: 2
    t.integer "UnitsOnOrder",    limit: 2
    t.integer "ReorderLevel",    limit: 2
    t.integer "Discontinued",               null: false
  end

  create_table "region", primary_key: "RegionID", id: :integer, limit: 2, force: :cascade do |t|
    t.string "RegionDescription", null: false
  end

  create_table "shippers", primary_key: "ShipperID", id: :integer, limit: 2, force: :cascade do |t|
    t.string "CompanyName", limit: 40, null: false
    t.string "Phone",       limit: 24
  end

  create_table "shippers_tmp", primary_key: "ShipperID", id: :integer, limit: 2, force: :cascade do |t|
    t.string "CompanyName", limit: 40, null: false
    t.string "Phone",       limit: 24
  end

  create_table "suppliers", primary_key: "SupplierID", id: :integer, limit: 2, force: :cascade do |t|
    t.string "CompanyName",  limit: 40, null: false
    t.string "ContactName",  limit: 30
    t.string "ContactTitle", limit: 30
    t.string "Address",      limit: 60
    t.string "City",         limit: 15
    t.string "Region",       limit: 15
    t.string "PostalCode",   limit: 10
    t.string "Country",      limit: 15
    t.string "Phone",        limit: 24
    t.string "Fax",          limit: 24
    t.text   "HomePage"
  end

  create_table "territories", primary_key: "TerritoryID", id: :string, limit: 20, force: :cascade do |t|
    t.string  "TerritoryDescription",           null: false
    t.integer "RegionID",             limit: 2, null: false
  end

  create_table "test", id: false, force: :cascade do |t|
    t.string "name", limit: 10
  end

  create_table "usstates", id: false, force: :cascade do |t|
    t.integer "StateID",     limit: 2,   null: false
    t.string  "StateName",   limit: 100
    t.string  "StateAbbr",   limit: 2
    t.string  "StateRegion", limit: 50
  end

end
