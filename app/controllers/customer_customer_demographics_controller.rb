class CustomerCustomerDemographicsController < ApplicationController
  before_action :set_customer_customer_demographic, only: [:show, :edit, :update, :destroy]

  # GET /customer_customer_demographics
  # GET /customer_customer_demographics.json
  def index
    @customer_customer_demographics = CustomerCustomerDemographic.all
  end

  # GET /customer_customer_demographics/1
  # GET /customer_customer_demographics/1.json
  def show
  end

  # GET /customer_customer_demographics/new
  def new
    @customer_customer_demographic = CustomerCustomerDemographic.new
  end

  # GET /customer_customer_demographics/1/edit
  def edit
  end

  # POST /customer_customer_demographics
  # POST /customer_customer_demographics.json
  def create
    @customer_customer_demographic = CustomerCustomerDemographic.new(customer_customer_demographic_params)

    respond_to do |format|
      if @customer_customer_demographic.save
        format.html { redirect_to @customer_customer_demographic, notice: 'Customer customer demographic was successfully created.' }
        format.json { render :show, status: :created, location: @customer_customer_demographic }
      else
        format.html { render :new }
        format.json { render json: @customer_customer_demographic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /customer_customer_demographics/1
  # PATCH/PUT /customer_customer_demographics/1.json
  def update
    respond_to do |format|
      if @customer_customer_demographic.update(customer_customer_demographic_params)
        format.html { redirect_to @customer_customer_demographic, notice: 'Customer customer demographic was successfully updated.' }
        format.json { render :show, status: :ok, location: @customer_customer_demographic }
      else
        format.html { render :edit }
        format.json { render json: @customer_customer_demographic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /customer_customer_demographics/1
  # DELETE /customer_customer_demographics/1.json
  def destroy
    @customer_customer_demographic.destroy
    respond_to do |format|
      format.html { redirect_to customer_customer_demographics_url, notice: 'Customer customer demographic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customer_customer_demographic
      @customer_customer_demographic = CustomerCustomerDemographic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customer_customer_demographic_params
      params.require(:customer_customer_demographic).permit(:Customer_id, :CustomerType_id)
    end
end
