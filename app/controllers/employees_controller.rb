class EmployeesController < ApplicationController
  before_action :find_employee, only: [:show, :edit, :update, :destroy]
  before_action :find_employees, only: [:index, :create, :edit, :update, :new]
  before_action :find_territories, only: [:create, :edit, :update, :new]

  def index
  end

  def show
  end

  def new
    @employee = Employee.new
  end

  def edit
    # byebug
  end

  def create
    @employee = Employee.new(employee_params)

    respond_to do |format|
      if @employee.save
        format.html { redirect_to @employee,
                                  notice: 'Employee was successfully created.' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to @employee,
                                  notice: 'Employee was successfully updated.' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to employees_url,
                                notice: 'Employee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def employee_params
    params.require(:employee).permit(:LastName, :FirstName, :Title,
                                     :TitleOfCourtesy, :BirthDate, :HireDate,
                                     :Address, :City, :Region, :PostalCode,
                                     :Country, :HomePhone, :Extension, :Photo,
                                     :Notes, :PhotoPath, :ReportsTo,
                                     :EmployeeID,
                                     territory_ids: [])
  end

  def find_employee
    @employee = Employee.find(params[:id])
  end

  def find_territories
    @all_territories = Territory.all
    @territories_with_description =
        @all_territories.map { |territory|
          [territory.TerritoryDescription, territory.TerritoryID]
        }
  end

  def find_employees
    @all_employees = Employee.all.includes(territories: [:region])
  end
end
