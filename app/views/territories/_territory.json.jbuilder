json.extract! territory, :id, :TerritoryDescription, :created_at, :updated_at
json.url territory_url(territory, format: :json)