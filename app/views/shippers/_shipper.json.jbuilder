json.extract! shipper, :id, :ShipperID, :CompanyName, :Phone, :created_at, :updated_at
json.url shipper_url(shipper, format: :json)