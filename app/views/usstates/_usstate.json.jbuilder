json.extract! usstate, :id, :StateName, :StateAbbr, :StateRegion, :created_at, :updated_at
json.url usstate_url(usstate, format: :json)