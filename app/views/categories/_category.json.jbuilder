json.extract! category, :id, :CategoryName, :Description, :Picture, :created_at, :updated_at
json.url category_url(category, format: :json)