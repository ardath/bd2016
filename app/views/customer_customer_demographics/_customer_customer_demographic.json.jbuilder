json.extract! customer_customer_demographic, :id, :Customer_id, :CustomerType_id, :created_at, :updated_at
json.url customer_customer_demographic_url(customer_customer_demographic, format: :json)