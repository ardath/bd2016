class Category < ApplicationRecord
  has_many :products, foreign_key: 'CategoryID'

  before_validation(on: :create) { bump_id }

  private

  def bump_id
    self.CategoryID = Category.maximum('CategoryID') + 1
  end
end

