class Territory < ApplicationRecord
  self.primary_key = 'TerritoryID'

  belongs_to :region, foreign_key: 'RegionID'

  has_and_belongs_to_many :employees,
                          join_table: 'employeeterritories',
                          foreign_key: 'TerritoryID',
                          association_foreign_key: 'EmployeeID'
end
