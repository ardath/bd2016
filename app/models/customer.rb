class Customer < ApplicationRecord
  self.primary_key = 'CustomerID'

  has_many :orders

  has_and_belongs_to_many :customer_demographics,
                          join_table: 'customercustomerdemo',
                          foreign_key: 'CustomerID',
                          association_foreign_key: 'CustomerTypeID'
end
