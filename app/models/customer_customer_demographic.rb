class CustomerCustomerDemographic < ApplicationRecord
  self.table_name = 'customercustomerdemo'

  belongs_to :customer
  belongs_to :customer_demographic
end
