class Order < ApplicationRecord
  # before_validation(on: :create) { bump_id }
  # after_create :bump_id

  # accepts_nested_attributes_for :products,
  #                               reject_if: :all_blank,
  #                               allow_destroy: true

  # has_many :order_details#, foreign_key: ['OrderID', 'ProductID'] do
  has_many :order_details, foreign_key: [:OrderID, :ProductID] do
    def formatted_details
      product_index = 1
      self.each_with_object([]) do |order_detail, details|
        details << '<p>%d. %s</p>' % [product_index, order_detail.formatted_product_detail]
        product_index += 1
      end.join("\n")
    end
  end
  accepts_nested_attributes_for :order_details

  has_many :products, through: :order_details #, foreign_key: [:OrderID, :ProductID]
  # accepts_nested_attributes_for :products

  belongs_to :customer, foreign_key: 'CustomerID'
  belongs_to :shipper, foreign_key: 'ShipVia'
  belongs_to :employee, foreign_key: 'EmployeeID'

  def employee_name
    employee ? employee.full_name : '-'
  end

  # private

  def bump_id
    self.OrderID = Order.maximum('OrderID') + 1
  end

end
