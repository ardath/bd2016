class Product < ApplicationRecord
  belongs_to :supplier, foreign_key: 'SupplierID'
  belongs_to :category, foreign_key: 'CategoryID'

  has_many :order_details, foreign_key: ['OrderID', 'ProductID']
  has_many :orders, through: :order_details

  before_validation(on: :create) { bump_id }

  private

  def bump_id
    self.ProductID = Product.maximum('ProductID') + 1
  end
end
