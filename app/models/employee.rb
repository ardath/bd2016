class Employee < ApplicationRecord
  self.primary_key = 'EmployeeID'

  before_validation(on: :create) { bump_id }

  belongs_to :supervisor, class_name: 'Employee', foreign_key: 'ReportsTo'
  has_many :subordinates, class_name: 'Employee', foreign_key: 'ReportsTo'

  has_many :orders

  has_and_belongs_to_many :territories, join_table: 'employeeterritories', \
    foreign_key: 'EmployeeID', association_foreign_key: 'TerritoryID' do
    def ids
      self.each_with_object([]) do |territory, territories_ids|
        territories_ids << territory.TerritoryID
      end
    end
  end

  def full_name
    [self.FirstName, self.LastName].join(' ')
  end

  def supervisor_name
    if supervisor && self_not_supervisor
      supervisor.full_name
    else
      '-'
    end
  end

  def supervisor_or_self_id
    supervisor ? supervisor.EmployeeID : self.EmployeeID
  end

  def formatted_territories
    territories.each_with_object([]) do |territory, territories|
      territories << territory_with_region(territory)
    end.join(',')
  end

  private

  def self_not_supervisor
    supervisor.full_name != self.full_name
  end

  def territory_with_region(territory)
    '%s(%s)' %
        [territory.TerritoryDescription, territory.region.RegionDescription]
  end

  def bump_id
    self.EmployeeID = Employee.maximum('EmployeeID') + 1
  end
end
