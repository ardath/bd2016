class OrderDetail < ApplicationRecord
  self.primary_keys = :OrderID, :ProductID

  belongs_to :order
  belongs_to :product

  def formatted_product_detail
    [
        product.ProductName,
        product_details
    ].join(': ')
  end

  private

  def product_details
    %w[ UnitPrice Quantity Discount ].each_with_object([]) do |detail, details|
      details << detail_with_name(detail)
    end.join(', ')
  end

  def detail_with_name(detail)
    '%s: %s' % [detail, self.send(detail)]
  end

end
