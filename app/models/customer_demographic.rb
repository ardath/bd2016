class CustomerDemographic < ApplicationRecord
  self.table_name = 'customerdemographics'
  self.primary_key = 'CustomerTypeID'

  has_and_belongs_to_many :customers,
                          join_table: :customercustomerdemo,
                          foreign_key: 'CustomerTypeID',
                          association_foreign_key: 'CustomerID'
end
