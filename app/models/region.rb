class Region < ApplicationRecord
  self.table_name = 'region'

  has_many :territories
end
