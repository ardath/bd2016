class Supplier < ApplicationRecord
  has_many :products, foreign_key: "SupplierID"
 before_validation(on: :create) { bump_id }

  private

  # incrementsa supplier's id
  def bump_id
    self.SupplierID = Supplier.maximum("SupplierID") + 1
  end
end
