class EmployeeTerritory < ApplicationRecord
  self.table_name = 'employeeterritories'

  belongs_to :employee
  belongs_to :territory

  accepts_nested_attributes_for :territory
end
