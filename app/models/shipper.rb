class Shipper < ApplicationRecord
  self.primary_key = 'ShipperID'
  before_validation(on: :create) { bump_id }

  has_many :orders

  private

  def bump_id
    self.ShipperID = Shipper.maximum('ShipperIZ') + 1
  end
end
