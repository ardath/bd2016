#!/usr/bin/bash

# install_dependencies.sh
#
# Install bd2016 project:
# * install required packages
# * clone bd2016 repository

install_packages_from_repository(){
  # sudo yum update
  # sudo yum update --skip-broken
  sudo yum install -y ${REQUIRED_PACKAGES[@]}
  install_result=$(rpm --query --queryformat "" ${REQUIRED_PACKAGES[@]})
  if [[ "x$install_result" == "x" ]]; then
    return 0
  fi
  export install_result
  return 1
}

clone_rails_app(){
    directory=$1
    git clone https://bitbucket.org/ardath/bd2016 ~vagrant/$directory
}
install_rvm_ruby(){
    gpg2 --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
 	curl -sSL https://get.rvm.io | bash -s stable --ruby
}

prepare_env(){
    export PATH=/home/vagrant/.rvm/gems/ruby-2.3.0/bin:/usr/local/bin:$PATH
    source /home/vagrant/.rvm/scripts/rvm
}

REQUIRED_PACKAGES=(
  git
  ruby
  postgresql
  nodejs
  patch
  libyaml-devel
  autoconf
  gcc-c++
  patch
  readline-devel
  zlib-devel
  libffi-devel
  openssl-devel
  automake
  libtool
  bison
  sqlite-devel
)

if install_packages_from_repository; then
  echo Packages installed: ${REQUIRED_PACKAGES[@]}
else
  echo Failed to install some packages from repository: $install_result
  exit 1
fi

prepare_env
#install_rvm_ruby
gem install bundler

if [[ ! -d bd2016 ]]; then
    clone_rails_app bd2016
fi

cd bd2016
bundle install

bundle exec rails server
